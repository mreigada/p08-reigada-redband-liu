#LineBoxer

Matt Reigada
	Gameplay Designer,
	author of edge, boardLocation, and board classes.
	co-author of gamescene class.

Tim Redband
	User Experience Designer,
	author of Menuscene, Buttoncontrol, and GameoverScene classes.
	co-author of gamescene class.

Shaoyang Liu
	Sound Designer,
	co-author of gamescene class.